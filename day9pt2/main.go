package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

type (
	Point struct {
		X int
		Y int
	}
	nothing struct{}
	Basin   map[Point]nothing
	Grid    []string
)

func (b Basin) Add(p Point) {
	b[p] = nothing{}
}

func (b Basin) Has(p Point) bool {
	_, found := b[p]
	return found
}

func search(grid Grid, p Point, basin Basin) {
	if p.X < 0 || p.X >= len(grid[0]) || p.Y < 0 || p.Y >= len(grid) {
		return
	}
	if grid[p.Y][p.X] == '9' {
		// We're at a boundary
		return
	}
	if basin.Has(p) {
		return
	}
	basin.Add(p)
	search(grid, Point{p.X + 1, p.Y}, basin)
	search(grid, Point{p.X - 1, p.Y}, basin)
	search(grid, Point{p.X, p.Y + 1}, basin)
	search(grid, Point{p.X, p.Y - 1}, basin)
}

func main() {
	f, err := os.OpenFile("input.txt", os.O_RDONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}
	s := bufio.NewScanner(f)
	grid := make([]string, 0)

	for s.Scan() {
		grid = append(grid, s.Text())
	}
	basins := make([]Basin, 0)
	for y := range grid {
		for x := range grid[y] {
			p := Point{x, y}
			found := false
			for _, b := range basins {
				found = b.Has(p)
				if found {
					break
				}
			}
			if found {
				log.Printf("Skipping: %v\n", p)
				continue
			}
			basin := Basin{}
			log.Printf("Searching: %v\n", p)
			search(grid, p, basin)
			if len(basin) > 0 {
				basins = append(basins, basin)
			}
		}
	}
	for i, b := range basins {
		for p := range b {
			grid[p.Y] = grid[p.Y][:p.X] + string(65+(i%26)) + grid[p.Y][p.X+1:]
		}
	}
	for _, l := range grid {
		fmt.Println(l)
	}
}

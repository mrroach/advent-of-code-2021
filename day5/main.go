package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

func drawHorizontal(space [][]int, x, y1, y2 int) {
	if y1 > y2 {
		y1, y2 = y2, y1
	}
	for i := y1; i <= y2; i++ {
		space[x][i] += 1
	}
}

func drawVertical(space [][]int, y, x1, x2 int) {
	if x1 > x2 {
		x1, x2 = x2, x1
	}
	for i := x1; i <= x2; i++ {
		space[i][y] += 1
	}
}
func drawDiagonal(space [][]int, x1, y1, x2, y2 int) {
	if x1 > x2 {
		x1, x2 = x2, x1
		y1, y2 = y2, y1
	}
	yIncr := 1
	if y1 > y2 {
		yIncr = -1
	}
	for i := 0; i <= x2-x1; i++ {
		space[x1+i][y1+yIncr*i] += 1
	}
}

func main() {
	f, err := os.OpenFile("input.txt", os.O_RDONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}
	s := bufio.NewScanner(f)
	space := make([][]int, 1000)
	for i := range space {
		space[i] = make([]int, 1000)
	}

	for s.Scan() {
		var x1, y1, x2, y2 int
		// lines look like 162,594 -> 162,448
		fmt.Sscanf(s.Text(), "%d,%d -> %d,%d", &x1, &y1, &x2, &y2)
		if x1 == x2 {
			drawHorizontal(space, x1, y1, y2)
		} else if y1 == y2 {
			drawVertical(space, y1, x1, x2)
		} else {
			drawDiagonal(space, x1, y1, x2, y2)
		}
	}
	count := 0
	for i := range space {
		for j := range space[i] {
			if space[i][j] > 1 {
				count++
			}
		}
	}
	fmt.Printf("Overlapping spaces: %d\n", count)
}

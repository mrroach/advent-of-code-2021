package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func spawn(fishes map[int]int) {
	newFishes := fishes[0]
	fishes[0] = 0
	for i := 1; i < 9; i++ {
		fishes[i-1] += fishes[i]
		fishes[i] = 0
	}
	fishes[6] += newFishes
	fishes[8] += newFishes
}

func count(fishes map[int]int) int {
	count := 0
	for i := range fishes {
		count += fishes[i]
	}
	return count
}

func main() {
	f, err := os.OpenFile("input.txt", os.O_RDONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}
	s := bufio.NewScanner(f)
	fishes := make(map[int]int) // days -> count

	for s.Scan() {
		initial := strings.Split(s.Text(), ",")
		for i := range initial {
			x, err := strconv.Atoi(initial[i])
			if err != nil {
				log.Fatal(err)
			}
			fishes[x]++
		}
	}
	fmt.Printf("Fishes: %d\n", len(fishes))
	for i := 0; i < 256; i++ {
		spawn(fishes)
		fmt.Printf("Day %d Fishes: %d\n", i+1, count(fishes))
	}
}

package main

import (
	"bufio"
	"log"
	"os"
	"strconv"
	"strings"
)

func cost(distance, c int) int {
	if distance == 0 {
		return 0
	}
	return c + cost(distance-1, c+1)
}

//   0:      1:      2:      3:      4:
//   aaaa    ....    aaaa    aaaa    ....
//  b    c  .    c  .    c  .    c  b    c
//  b    c  .    c  .    c  .    c  b    c
//   ....    ....    dddd    dddd    dddd
//  e    f  .    f  e    .  .    f  .    f
//  e    f  .    f  e    .  .    f  .    f
//   gggg    ....    gggg    gggg    ....

//    5:      6:      7:      8:      9:
//   aaaa    aaaa    aaaa    aaaa    aaaa
//  b    .  b    .  .    c  b    c  b    c
//  b    .  b    .  .    c  b    c  b    c
//   dddd    dddd    ....    dddd    dddd
//  .    f  e    f  .    f  e    f  .    f
//  .    f  e    f  .    f  e    f  .    f
//   gggg    gggg    ....    gggg    gggg

func main() {
	f, err := os.OpenFile("input.txt", os.O_RDONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}
	s := bufio.NewScanner(f)
	positions := make([]int, 0)

	largest := 0
	smallest := 0
	for s.Scan() {
		initial := strings.Split(s.Text(), ",")
		for i := range initial {
			x, err := strconv.Atoi(initial[i])
			if err != nil {
				log.Fatal(err)
			}
			positions = append(positions, x)
			if x > largest {
				largest = x
			}
			if x < smallest {
				smallest = x
			}
		}
	}
	for i := smallest; i <= largest; i++ {
		c := 0
		for j := range positions {
			if i < positions[j] {
				c += cost(positions[j]-i, 1)
			} else if i > positions[j] {
				c += cost(i-positions[j], 1)
			}
		}
		log.Printf("Position: %d, cost: %d", i, c)
	}
}

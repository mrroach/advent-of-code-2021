package main

import (
	"bufio"
	"log"
	"os"
	"strconv"
)

func main() {
	f, err := os.OpenFile("input.txt", os.O_RDONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}
	s := bufio.NewScanner(f)
	grid := make([]string, 0)

	for s.Scan() {
		grid = append(grid, s.Text())
	}
	sum := 0
	for y := range grid {
		for x := range grid[y] {
			// Test left
			if x-1 >= 0 && grid[y][x-1] <= grid[y][x] {
				continue
			}
			// Test up
			if y-1 >= 0 && grid[y-1][x] <= grid[y][x] {
				continue
			}
			// Test right
			if x+1 < len(grid[y]) && grid[y][x+1] <= grid[y][x] {
				continue
			}
			if y+1 < len(grid) && grid[y+1][x] <= grid[y][x] {
				continue
			}
			val, err := strconv.Atoi(string(grid[y][x]))
			if err != nil {
				log.Fatal(err)
			}
			sum += val + 1
			log.Printf("%d,%d (%d) is a low point (sum: %d)\n", x, y, val, sum)
		}
	}
}

package main

import (
	"bufio"
	"fmt"
	"log"
	"math"
	"os"
)

func main() {
	f, err := os.OpenFile("input.txt", os.O_RDONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}
	zeroes := make([]int, 12)
	ones := make([]int, 12)
	gamma := 0
	epsilon := 0
	s := bufio.NewScanner(f)
	for s.Scan() {
		value := s.Text()
		for i := 0; i < 12; i++ {
			switch value[i] {
			case '0':
				zeroes[i]++
			case '1':
				ones[i]++
			}
		}
	}
	for i := 0; i < 12; i++ {
		if zeroes[i] > ones[i] {
			epsilon += int(math.Pow(2, 11.0-float64(i)))
		} else {
			gamma += int(math.Pow(2, 11.0-float64(i)))
		}
	}
	fmt.Printf("Zeroes: %v\n", zeroes)
	fmt.Printf("Ones: %d\n", ones)
	fmt.Printf("epsilon: %d\n", epsilon)
	fmt.Printf("gamma: %d\n", gamma)
	fmt.Printf("product: %d\n", gamma*epsilon)
}

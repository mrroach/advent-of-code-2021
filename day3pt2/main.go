package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func filterValues(values []string, idx int, val byte) []string {
	if len(values) == 1 {
		return values
	}
	filtered := make([]string, 0)
	for _, s := range values {
		if s[idx] == val {
			filtered = append(filtered, s)
		}
	}
	return filtered
}

func frequency(values []string) ([]int, []int) {
	zeroes := make([]int, 12)
	ones := make([]int, 12)
	for _, value := range values {
		for i := 0; i < 12; i++ {
			switch value[i] {
			case '0':
				zeroes[i]++
			case '1':
				ones[i]++
			}
		}
	}
	return zeroes, ones
}

func main() {
	f, err := os.OpenFile("input.txt", os.O_RDONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}
	values := make([]string, 0)
	var epsilon, gamma int

	s := bufio.NewScanner(f)
	for s.Scan() {
		value := s.Text()
		values = append(values, value)
	}
	oValues := values
	cValues := values
	zeroes, ones := frequency(values)

	for i := 0; i < 12; i++ {
		if zeroes[i] > ones[i] {
			oValues = filterValues(oValues, i, '0')
		} else if ones[i] > zeroes[i] {
			oValues = filterValues(oValues, i, '1')
		} else {
			oValues = filterValues(oValues, i, '1')
		}
		zeroes, ones = frequency(oValues)
	}
	zeroes, ones = frequency(values)
	for i := 0; i < 12; i++ {
		if zeroes[i] > ones[i] {
			cValues = filterValues(cValues, i, '1')
		} else if ones[i] > zeroes[i] {
			cValues = filterValues(cValues, i, '0')
		} else {
			cValues = filterValues(cValues, i, '0')
		}
		zeroes, ones = frequency(cValues)
	}
	oxygen, err := strconv.ParseInt(oValues[0], 2, 64)
	if err != nil {
		log.Fatal(err)
	}
	co2, err := strconv.ParseInt(cValues[0], 2, 64)
	if err != nil {
		log.Fatal(err)
	}
	lifeSupport := co2 * oxygen
	fmt.Printf("Zeroes: %v\n", zeroes)
	fmt.Printf("Ones: %d\n", ones)
	fmt.Printf("epsilon: %d\n", epsilon)
	fmt.Printf("gamma: %d\n", gamma)
	fmt.Printf("product: %d\n", gamma*epsilon)
	fmt.Printf("oValues: %v\n", oValues)
	fmt.Printf("cValues: %v\n", cValues)
	fmt.Printf("lifeSupport: %d\n", lifeSupport)
}

package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func spawn(fishes []int) []int {
	count := len(fishes)
	for i := 0; i < count; i++ {
		if fishes[i] == 0 {
			fishes = append(fishes, 8)
			fishes[i] = 6
		} else {
			fishes[i]--
		}
	}
	return fishes
}

func main() {
	f, err := os.OpenFile("input.txt", os.O_RDONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}
	s := bufio.NewScanner(f)
	fishes := make([]int, 0)

	for s.Scan() {
		initial := strings.Split(s.Text(), ",")
		for i := range initial {
			x, err := strconv.Atoi(initial[i])
			if err != nil {
				log.Fatal(err)
			}
			fishes = append(fishes, x)
		}
	}
	fmt.Printf("Fishes: %d\n", len(fishes))
	for i := 0; i < 256; i++ {
		fishes = spawn(fishes)
		fmt.Printf("Day %d Fishes: %d\n", i+1, len(fishes))
	}

}

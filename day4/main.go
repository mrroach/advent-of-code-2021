package main

import (
	"bufio"
	"log"
	"os"
	"strconv"
	"strings"
)

type Square struct {
	Value  int
	Marked bool
}

type Row []Square
type Board []Row

func (b Board) Clear() {
	for i := 0; i < len(b); i++ {
		for j := 0; j < len(b[i]); j++ {
			b[i][j].Value = -1
			b[i][j].Marked = false
		}
	}
}

func (b Board) RowWins(row int) bool {
	for _, square := range b[row] {
		if !square.Marked {
			return false
		}
	}
	return true
}

func (b Board) ColumnWins(col int) bool {
	for i := 0; i < len(b); i++ {
		if !b[i][col].Marked {
			return false
		}
	}
	return true
}

func (b Board) IsWinner() bool {
	for i := 0; i < len(b); i++ {
		if b.RowWins(i) {
			return true
		}
	}
	for i := 0; i < len(b[0]); i++ {
		if b.ColumnWins(i) {
			return true
		}
	}
	return false
}

func (b Board) MarkValue(val int) {
	for i := 0; i < len(b); i++ {
		for j := 0; j < len(b[i]); j++ {
			if b[i][j].Value == val {
				b[i][j].Marked = true
			}
		}
	}
}

func (b Board) SumOfUnmarked() int {
	sum := 0
	for i := 0; i < len(b); i++ {
		for j := 0; j < len(b[i]); j++ {
			if !b[i][j].Marked {
				sum += b[i][j].Value
			}
		}
	}
	return sum
}

func FindWinner(val int, boards *[]Board) (bool, int) {
	for i, board := range *boards {
		if len(board) == 1 {
			log.Printf("Aargh, board %d is empty\n", i)
			continue
		}
		if board.IsWinner() {
			log.Printf("Winner! Board %d", i)
			return true, i
		}
	}
	return false, 0
}

func main() {
	f, err := os.OpenFile("input.txt", os.O_RDONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}
	s := bufio.NewScanner(f)
	s.Scan()
	numbers := strings.Split(s.Text(), ",")
	log.Printf("%v", numbers)
	var boards []Board
	b := make(Board, 0)

	for s.Scan() {
		value := s.Text()
		if len(value) < 3 && len(b) > 0 {
			boards = append(boards, b)
			b = make(Board, 0)
			continue
		}
		fields := strings.Fields(value)
		row := make(Row, len(fields))
		for i, field := range fields {
			val, err := strconv.Atoi(field)
			if err != nil {
				log.Fatal(err)
			}
			row[i].Value = val
			row[i].Marked = false
		}
		if len(row) == 0 {
			log.Printf("Empty row: %s\n", value)
			continue
		}
		b = append(b, row)
	}
	for _, num := range numbers {
		val, err := strconv.Atoi(num)
		if err != nil {
			log.Fatal(err)
		}
		for _, board := range boards {
			board.MarkValue(val)
		}
		winner, idx := FindWinner(val, &boards)
		for winner {
			log.Printf("Found winner. Board %d. Is winner? %v Winning number: %d", idx, boards[idx].IsWinner(), val)
			log.Printf("Sum of unmarked: %d. Score: %d\n", boards[idx].SumOfUnmarked(), boards[idx].SumOfUnmarked()*val)
			boards[idx].Clear()
			winner, idx = FindWinner(val, &boards)
		}
	}
}

package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	depth := 0
	position := 0
	aim := 0
	f, err := os.OpenFile("input.txt", os.O_RDONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}
	s := bufio.NewScanner(f)
	for i := 0; s.Scan(); i++ {
		fields := strings.Fields(s.Text())
		if len(fields) != 2 {
			log.Fatal("Wrong field length '%s", s.Text())
		}
		distance, err := strconv.Atoi(fields[1])
		if err != nil {
			log.Fatal(err)
		}
		fmt.Printf("Command: %s %d\n", fields[0], distance)
		switch fields[0] {
		case "up":
			aim -= distance
		case "down":
			aim += distance
		case "forward":
			position += distance
			depth += aim * distance
		case "back":
			position -= distance
		default:
			log.Fatalf("Unrecognized command: %s", fields[0])
		}
		fmt.Printf("Aim %d Depth %d position %d\n", aim, depth, position)
	}
	fmt.Printf("Final depth %d position %d\n", depth, position)
	fmt.Printf("Product: %d\n", depth*position)
}

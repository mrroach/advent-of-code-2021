package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

func main() {
	depth := 0
	horizontal := 0
	f, err := os.OpenFile("input.txt", os.O_RDONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}
	s := bufio.NewScanner(f)
	inc := 0
	prev := 0
	for i := 0; s.Scan(); i++ {
		fields := strings.Fields(s.Text())
		if err != nil {
			log.Fatal(err)
		}
		if i == 0 {
			prev = current
			continue
		}
		if current > prev {
			inc++
		}
		prev = current
	}
	fmt.Printf("Found %d increments\n", inc)
}

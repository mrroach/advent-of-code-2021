package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func main() {
	f, err := os.OpenFile("input.txt", os.O_RDONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}
	s := bufio.NewScanner(f)
	inc := 0
	values := make([]int, 0)

	for s.Scan() {
		current, err := strconv.Atoi(s.Text())
		if err != nil {
			log.Fatal(err)
		}
		values = append(values, current)
	}
	for i := 0; i+3 < len(values); i++ {
		current := values[i] + values[i+1] + values[i+2]
		next := values[i+1] + values[i+2] + values[i+3]
		if next > current {
			inc++
			log.Printf("%d + %d + %d = %d is greater than %d + %d + %d = %d\n", values[i], values[i+1], values[i+2], current, values[i+1], values[i+2], values[i+3], next)
		}
	}
	fmt.Printf("Found %d increments\n", inc)
}
